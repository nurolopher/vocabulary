package com.nurolopher.toeflvocabulary.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nurolopher.toeflvocabulary.R;
import com.nurolopher.toeflvocabulary.WordListActivity;
import com.nurolopher.toeflvocabulary.model.Word;
import com.nurolopher.toeflvocabulary.viewholder.CompleteListViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nurolopher on 11/8/2015.
 */
public class WordListAdapter extends RecyclerView.Adapter<CompleteListViewHolder> {

    public int currentFilter;
    private List<Word> words;
    private List<Word> wordsAll;
    private Context context;

    public WordListAdapter(Context context, List<Word> words) {
        this.words = words;
        this.wordsAll = Word.listAll(Word.class);
        currentFilter = Word.NEW;
        this.context = context;
    }


    private void handleActions(CompleteListViewHolder viewHolder, final Word word, final int position) {
        final ImageView actionView = viewHolder.actionView;
        if (word.getStatus() == Word.MEMORIZED) {
            actionView.setImageResource(R.drawable.ic_restore_48dp);
            actionView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    word.setStatus(Word.NEW);
                    word.save();
                    if (currentFilter != Word.ALL) {
                        words.remove(word);
                        ((WordListActivity) context).showSnackBar(word, position);
                    }
                    notifyDataSetChanged();
                }
            });
        } else {
            actionView.setImageResource(R.drawable.ic_done_48dp);
            actionView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    word.setStatus(Word.MEMORIZED);
                    word.save();
                    if (currentFilter != Word.ALL) {
                        words.remove(word);
                        ((WordListActivity) context).showSnackBar(word, position);
                    }
                    notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public CompleteListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(context)
                .inflate(R.layout.wod_list_row, parent, false);
        return new CompleteListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CompleteListViewHolder holder, int position) {
        final Word word = words.get(position);


        holder.titleView.setText(word.getTitle());
        holder.definitionView.setText(word.getDefinition());
        holder.exampleView.setText(word.getExample());
        holder.index.setText(String.format("%d", position + 1));
        holder.statusView.setText(word.getStatusAsString(currentFilter, context));
        String questionStatus = context.getResources().getString(R.string.question_status_memorized);
        if (word.getStatus() == Word.NEW) {
            questionStatus = context.getResources().getString(R.string.question_status_new);
        }
        holder.statusQuestion.setText(questionStatus);
        holder.statusView.setVisibility((currentFilter != Word.ALL) ? View.GONE : View.VISIBLE);
        handleActions(holder, word, position);
    }

    @Override
    public int getItemCount() {
        return words.size();
    }

    public void filter(int status) {
        currentFilter = status;
        if (status == Word.ALL) {
            words = wordsAll;
        } else {
            words = new ArrayList<>();
            for (Word word : wordsAll) {
                if (word.getStatus() == status) {
                    words.add(word);
                }
            }
        }
        notifyDataSetChanged();
    }
}

