package com.nurolopher.toeflvocabulary.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nurolopher.toeflvocabulary.R;

/**
 * Created by nurolopher on 11/11/2015.
 */
public class CompleteListViewHolder extends RecyclerView.ViewHolder {
    public TextView titleView;
    public TextView definitionView;
    public TextView exampleView;
    public ImageView actionView;
    public TextView statusView;
    public TextView statusQuestion;
    public TextView index;

    public CompleteListViewHolder(View base) {
        super(base);
        titleView = (TextView) base.findViewById(R.id.word_title);
        definitionView = (TextView) base.findViewById(R.id.word_definition);
        exampleView = (TextView) base.findViewById(R.id.example);
        statusView = (TextView) base.findViewById(R.id.word_status);
        actionView = (ImageView) base.findViewById(R.id.word_action);
        statusQuestion = (TextView) base.findViewById(R.id.status_question);
        index = (TextView) base.findViewById(R.id.index);
    }
}
